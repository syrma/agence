﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Types;
namespace CInterface
{
    public interface IGestionC
    {
        Voiture[] ListeVoitures(DateTime date);
        // true if the operation is successful, false otherwise
        bool Reserver(int numImmatriculation, int id, int nombreJours, DateTime dateDebut);
        
        bool Inscrire(string nom, string password, string numTelephone);

        //fonctionnalité additionnelle
        int Authentifier(string numTel, string password);
    }
}
