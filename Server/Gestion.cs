using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CInterface;
using GInterface;
using Types;
using MySql.Data.MySqlClient;
using System.Data;


namespace ServeurAgence
{
    class Gestion : MarshalByRefObject, IGestionC, IGestionG, ScheduleService.EventGestion
    {
        // donnes ncessaires  la connexion  la base de donnes
        internal const string DATABASE_SERVER = "10.0.2.2";
        internal const string CONNECTION_STRING = "Server=" + DATABASE_SERVER + ";Database=RemotingAG;uid=dbuser;password=YFht754;";

        // requtes SQL des diffrents services

        //client
        const string SQL_INSCRIRE_CLIENT = "INSERT INTO Clients(nom,password,numTel,bloque) VALUES(@nom,@password,@numTel,0);";

        const string SQL_CONSULTER_LISTE_VOITURE = "SELECT DISTINCT Voitures.numImmatriculation, marque, modele, tarif FROM Voitures LEFT JOIN Reservations on Voitures.numImmatriculation = Reservations.numImmatriculation where dateReservation IS NULL OR NOT EXISTS (SELECT * FROM Reservations WHERE (@Date >= dateReservation AND @Date < DATE_ADD(dateReservation, INTERVAL nombreJoursReservation DAY))) ;";
        const string SQL_CONSULTER_LISTE_VOITURE2 = "SELECT Voitures.numImmatriculation,marque,modele,tarif, MAX(CURRENT_DATE BETWEEN dateReservation AND DATE_ADD(dateReservation, INTERVAL nombreJoursReservation-1 DAY)) as 'etat' FROM Voitures LEFT JOIN Reservations ON Voitures.numImmatriculation = Reservations.numImmatriculation GROUP BY Voitures.numImmatriculation;";

        const string SQL_RESERVER_VOITURE1 = "INSERT INTO Reservations SELECT @numImmatriculation,@id,@dateReservation,@nombreJoursReservation FROM dual WHERE NOT EXISTS (SELECT * from Reservations WHERE numImmatriculation = @numImmatriculation AND ((@dateReservation >= dateReservation AND @dateReservation < DATE_ADD(dateReservation, INTERVAL nombreJoursReservation DAY)) OR dateReservation BETWEEN @dateReservation AND DATE_ADD(@dateReservation,INTERVAL @nombreJoursReservation-1 DAY)));";
        //const string SQL_RESERVER_VOITURE1 = "INSERT INTO Reservations(numImmatriculation,id,dateReservation,nombreJoursReservation) VALUES(@numImmatriculation,@id,@dateReservation,@nombreJoursReservation)";

        //gestionnaire

        const string SQL_INSERER_VOITURE = "INSERT INTO Voitures VALUES(@numImmatriculation,@marque,@modele,@tarif);";

        const string SQL_SUPPRIMER_VOITURE = "DELETE FROM Voitures WHERE numImmatriculation = @numImmatriculation";

        const string SQL_BLOQUER_CLIENT = "UPDATE Clients SET bloque = 1 WHERE id=@id";
        const string SQL_BLOQUER_CLIENT2 = "DELETE FROM Reservations WHERE id=@id";

        const string SQL_RECETTE = "SELECT SUM(tarif * nombreJoursReservation) FROM Voitures INNER JOIN Reservations ON Voitures.numImmatriculation = Reservations.numImmatriculation AND dateReservation = @Date;";

		const string SQL_CLIENTS_RETARDATAIRES = "SELECT DISTINCT Clients.id, nom, numTel FROM Clients INNER JOIN Reservations ON Clients.id = Reservations.id and CURRENT_DATE >= DATE_ADD(Reservations.dateReservation, INTERVAL nombreJoursReservation-1 DAY) WHERE bloque = 0;";
        
        //fonctionnalits additionnelles 

        const string SQL_AUTHENTIFIER = "SELECT * FROM Clients WHERE numTel=@numTel AND bloque = 0;";

        const string SQL_LIBERER_VOITURE = "DELETE from Reservations WHERE numImmatriculation = @numImmatriculation AND dateReservation <= CURRENT_DATE ; ";

        //interface gestionnaire
        public bool Bloquer(int id)
        {
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection(CONNECTION_STRING);
                conn.Open();
                MySqlCommand cmc = new MySqlCommand(SQL_BLOQUER_CLIENT, conn);
                cmc.Parameters.AddWithValue("@id", id);
                MySqlCommand cmc2 = new MySqlCommand(SQL_BLOQUER_CLIENT2, conn);
                cmc2.Parameters.AddWithValue("@id", id);
                return (cmc.ExecuteNonQuery() > 0 && cmc.ExecuteNonQuery() > 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        public double Recette(DateTime date)
        {
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection(CONNECTION_STRING);
                conn.Open();
                MySqlCommand cmc = new MySqlCommand(SQL_RECETTE, conn);
                cmc.Parameters.AddWithValue("@Date", date);
                return (double.Parse(cmc.ExecuteScalar() + ""));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return -1;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        public bool Inserer(Voiture voiture)
        {
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection(CONNECTION_STRING);
                conn.Open();
                Console.WriteLine(conn);
                MySqlCommand cmc = new MySqlCommand(SQL_INSERER_VOITURE, conn);
                cmc.Parameters.AddWithValue("@numImmatriculation", voiture.NumImmatriculation);
                cmc.Parameters.AddWithValue("@marque", voiture.Marque);
                cmc.Parameters.AddWithValue("@modele", voiture.Modele);
                cmc.Parameters.AddWithValue("@tarif", voiture.Tarif);
                return (cmc.ExecuteNonQuery() > 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public bool Supprimer(int numImmatriculation)
        {
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection(CONNECTION_STRING);
                conn.Open();
                Console.WriteLine(conn);
                MySqlCommand cmc = new MySqlCommand(SQL_SUPPRIMER_VOITURE, conn);
                cmc.Parameters.AddWithValue("@numImmatriculation", numImmatriculation);
                return (cmc.ExecuteNonQuery() > 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public Voiture[] ListeVoitures()
        {
            List<Voiture> voitures = new List<Voiture>();
            MySqlConnection conn = null;
            try
            {

                conn = new MySqlConnection(CONNECTION_STRING);
                conn.Open();
                MySqlCommand cmc = new MySqlCommand(SQL_CONSULTER_LISTE_VOITURE2, conn);
                MySqlDataReader datareader = cmc.ExecuteReader();
                while (datareader.Read())
                {
                    bool etat = (datareader["etat"] + "").Equals("1") ? true : false ;
                    Voiture voiture = new Voiture(Int32.Parse(datareader["numImmatriculation"] + ""), datareader["marque"] + "", datareader["modele"] + "", double.Parse(datareader["tarif"] + ""), etat);
                    voitures.Add(voiture);
                }
                return voitures.ToArray();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }
        public bool Liberer(int numImmatriculation)
        {
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection(CONNECTION_STRING);
                conn.Open();
                Console.WriteLine(conn);
                MySqlCommand cmc = new MySqlCommand(SQL_LIBERER_VOITURE, conn);
                cmc.Parameters.AddWithValue("@numImmatriculation", numImmatriculation);
                return (cmc.ExecuteNonQuery() > 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        //interface client
        public bool Inscrire(string nom, string password, string numTelephone)
        {
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection(CONNECTION_STRING);
                conn.Open();
                MySqlCommand cmc = new MySqlCommand(SQL_INSCRIRE_CLIENT, conn);
                cmc.Parameters.AddWithValue("@nom", nom);
                cmc.Parameters.AddWithValue("@password", password);
                cmc.Parameters.AddWithValue("@numTel", numTelephone);

                return (cmc.ExecuteNonQuery() > 0);
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public bool Reserver(int numImmatriculation, int id, int nombreJours, DateTime dateDebut)
        {
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection(CONNECTION_STRING);
                conn.Open();
                Console.WriteLine(conn);
                MySqlCommand cmc = new MySqlCommand(SQL_RESERVER_VOITURE1, conn);
                //@numImmatriculation,@id,@dateReservation,@nombreJoursReservation
                cmc.Parameters.AddWithValue("@numImmatriculation", numImmatriculation);
                cmc.Parameters.AddWithValue("@id", id);
                cmc.Parameters.AddWithValue("@dateReservation", dateDebut);
                cmc.Parameters.AddWithValue("@nombreJoursReservation", nombreJours);
                return (cmc.ExecuteNonQuery() > 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        public Voiture[] ListeVoitures(DateTime date)
        {
            Console.WriteLine(date);
            List<Voiture> voitures = new List<Voiture>();
            MySqlConnection conn = null;
            try
            {

                conn = new MySqlConnection(CONNECTION_STRING);
                conn.Open();
                MySqlCommand cmc = new MySqlCommand(SQL_CONSULTER_LISTE_VOITURE, conn);
                cmc.Parameters.AddWithValue("@Date", date);
                MySqlDataReader datareader = cmc.ExecuteReader();
                while (datareader.Read())
                {
                    Voiture voiture = new Voiture(Int32.Parse(datareader["numImmatriculation"] + ""), datareader["marque"] + "", datareader["modele"] + "", double.Parse(datareader["tarif"] + ""), false);
                    voitures.Add(voiture);
                }
                return voitures.ToArray();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public int Authentifier(string numTel, string password)
        {
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection(CONNECTION_STRING);
                conn.Open();
                Console.WriteLine(conn);
                MySqlCommand cmc = new MySqlCommand(SQL_AUTHENTIFIER, conn);
                cmc.Parameters.AddWithValue("@numTel", numTel);
                MySqlDataReader datareader = cmc.ExecuteReader();
                if (datareader.Read())
                {
                    String pass = datareader["password"] + "";
                    int id = Int32.Parse(datareader["id"] + "");
                    if (pass != null & pass.Equals(password))
                    {
                        return id;
                    }
                }
                return -1;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return -1;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

        }
        public void EventInvoker()
        {
            if (ListeClientsRetardataires == null)
                return;

            Console.WriteLine("Pr�paration de la liste de clients � envoyer au gestionnaire...");
            List<Client> retardataires = new List<Client>();
            MySqlConnection conn = null;
            try
            {
                    conn = new MySqlConnection(CONNECTION_STRING);
                    conn.Open();
                    MySqlCommand cmc = new MySqlCommand(SQL_CLIENTS_RETARDATAIRES, conn);
                    MySqlDataReader datareader = cmc.ExecuteReader();
                    while (datareader.Read())
                    {
                        Client client = new Client(Int32.Parse(datareader["id"] + ""), datareader["nom"] + "", datareader["numTel"] + "");
                        retardataires.Add(client);
                    }
                    if (retardataires.Count== 0)
                    {
                        Console.WriteLine("Aucun client retardataire, rien ne sera envoy� au gestionnaire.");
                        return;
                    }

                    ListeClients clientsdelegate = null;
                    Delegate[] invocationList_ = null;
                    try
                    {
                        invocationList_ = ListeClientsRetardataires.GetInvocationList();
                    }
                    catch (MemberAccessException ex)
                    {
                        throw ex;
                    }
                    if (invocationList_ != null)
                    {
                        lock(this)
                        {
                            foreach(Delegate del in invocationList_)
                            {
                                try
                                {
                                    clientsdelegate = (ListeClients)del;
                                    clientsdelegate(retardataires.ToArray());
                                }
                                catch (Exception e)
                                {
                                    ListeClientsRetardataires -= clientsdelegate;
                                }
                            }
                        }
                    }
                    Console.WriteLine("La liste des clients a �t� envoy�e avec succ�s.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


        public event ListeClients ListeClientsRetardataires = null;
    }

}

