﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting;
using System.Collections;

namespace ServeurAgence
{
    class ServeurAgence
    {
    [STAThread]
        static void Main(string[] args)
        {
            try {
                SoapServerFormatterSinkProvider serverProvider = new SoapServerFormatterSinkProvider();
                serverProvider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
                IDictionary props = new Hashtable();
                props["port"] = 1069;
                HttpChannel canal = new HttpChannel(props, null, serverProvider);
                ChannelServices.RegisterChannel(canal, false);
                RemotingConfiguration.RegisterWellKnownServiceType(typeof(Gestion), "GestClients", WellKnownObjectMode.Singleton);
                Console.WriteLine("Serveur démarré.");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadLine();
			}
		}
		
	}
}

		