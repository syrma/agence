﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CInterfaceGestReservations
{
    public interface IGestReservationsC
    {
        // true if the operation is successful, false otherwise
        bool Reserver(int numImmatriculation, int id, int nombreJours, DateTime dateDebut);
    }
}
