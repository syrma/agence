﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Types;
using NCron.Service;
using NCron.Fluent.Crontab;
using NCron.Fluent.Generics;

namespace ScheduleService
{
    class ScheduleService: NCron.CronJob
	{
        public override void Execute()
        {
            Console.WriteLine("Test NCron");
            EventGestion ctl = (EventGestion)Activator.GetObject(typeof(EventGestion), "http://localhost:1069/GestClients");
            ctl.EventInvoker();
        }

        static void Main(string[] args)
        {
            Bootstrap.Init(args, ServiceSetup);
        }

        static void ServiceSetup(SchedulingService service)
        {
            service.At("0 17 * * *").Run<ScheduleService>();
        }

    }
}
