﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GInterfaceGestReservations
{
    public interface IGestReservationsG
    {
        double Recette(DateTime date);
    }
}
