﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Types;
using GInterface;

namespace Gestionnaire
{
    public partial class FormListeRetard : Form
    {
    IGestionG ctl ;
        public FormListeRetard(Client[] retard, IGestionG ctl)
        {
            InitializeComponent();
            this.ctl = ctl ; 
            listView1.Items.Clear();
            foreach (Client c in retard)
            {
                listView1.Items.Add(Convert.ToString(c.Id)).SubItems.AddRange(new string[] {c.Nom, c.NumTel});

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count <= 0)
            {
                MessageBox.Show("Veuillez sélectionner un client.");
                return;
            }
            try
            {
                //Recuperer le numero d'immatriculation d'un element selectionné 
                ListViewItem v = listView1.SelectedItems[0];
                Console.WriteLine(v.SubItems[0].Text);
                int aSup = Int32.Parse(v.SubItems[0].Text);//cast en int
                MessageBox.Show(ctl.Bloquer(aSup) ? "Ce client est désormais marqué comme bloqué" : "Une erreur a eu lieu (consulter serveur)");

                //Actualiser la liste
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("Veuillez sélectionner un élément.");

            }
            listView1.Items.RemoveAt((((ListViewItem)listView1.SelectedItems[0]).Index));
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
