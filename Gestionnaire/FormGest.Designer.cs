﻿namespace Gestionnaire
{
    partial class FormGest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.butLiberer = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.butCalcul = new System.Windows.Forms.Button();
            this.butSuppressionV = new System.Windows.Forms.Button();
            this.butAjoutV = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.butConsulter = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel1.Controls.Add(this.butConsulter);
            this.panel1.Controls.Add(this.butLiberer);
            this.panel1.Controls.Add(this.listView1);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.butCalcul);
            this.panel1.Controls.Add(this.butSuppressionV);
            this.panel1.Controls.Add(this.butAjoutV);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(0, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(544, 434);
            this.panel1.TabIndex = 0;
            // 
            // butLiberer
            // 
            this.butLiberer.Location = new System.Drawing.Point(12, 57);
            this.butLiberer.Name = "butLiberer";
            this.butLiberer.Size = new System.Drawing.Size(94, 38);
            this.butLiberer.TabIndex = 8;
            this.butLiberer.Text = "Liberer une voiture";
            this.butLiberer.UseVisualStyleBackColor = true;
            this.butLiberer.Click += new System.EventHandler(this.butLiberer_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.listView1.Location = new System.Drawing.Point(12, 140);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(517, 279);
            this.listView1.TabIndex = 7;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Numéro d\'immatriculation";
            this.columnHeader1.Width = 134;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Marque";
            this.columnHeader2.Width = 95;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Modèle";
            this.columnHeader3.Width = 91;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Tarif";
            this.columnHeader4.Width = 104;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Etat";
            this.columnHeader5.Width = 105;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(160, 96);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(216, 20);
            this.dateTimePicker1.TabIndex = 6;
            // 
            // butCalcul
            // 
            this.butCalcul.Location = new System.Drawing.Point(429, 52);
            this.butCalcul.Name = "butCalcul";
            this.butCalcul.Size = new System.Drawing.Size(100, 38);
            this.butCalcul.TabIndex = 5;
            this.butCalcul.Text = "Calculer la recette d\'une journée";
            this.butCalcul.UseVisualStyleBackColor = true;
            this.butCalcul.Click += new System.EventHandler(this.butCalcul_Click);
            // 
            // butSuppressionV
            // 
            this.butSuppressionV.Location = new System.Drawing.Point(12, 13);
            this.butSuppressionV.Name = "butSuppressionV";
            this.butSuppressionV.Size = new System.Drawing.Size(94, 38);
            this.butSuppressionV.TabIndex = 3;
            this.butSuppressionV.Text = "Supprimer voiture";
            this.butSuppressionV.UseVisualStyleBackColor = true;
            this.butSuppressionV.Click += new System.EventHandler(this.butSuppressionV_Click);
            // 
            // butAjoutV
            // 
            this.butAjoutV.Location = new System.Drawing.Point(429, 9);
            this.butAjoutV.Name = "butAjoutV";
            this.butAjoutV.Size = new System.Drawing.Size(100, 37);
            this.butAjoutV.TabIndex = 2;
            this.butAjoutV.Text = "Insérer une nouvelle voiture";
            this.butAjoutV.UseVisualStyleBackColor = true;
            this.butAjoutV.Click += new System.EventHandler(this.butAjoutV_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel2.BackgroundImage = global::Gestionnaire.Properties.Resources._2320995117_small_1;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Location = new System.Drawing.Point(118, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(305, 90);
            this.panel2.TabIndex = 0;
           
            // 
            // butConsulter
            // 
            this.butConsulter.Location = new System.Drawing.Point(429, 96);
            this.butConsulter.Name = "butConsulter";
            this.butConsulter.Size = new System.Drawing.Size(100, 34);
            this.butConsulter.TabIndex = 9;
            this.butConsulter.Text = "Consulter la liste des voitures";
            this.butConsulter.UseVisualStyleBackColor = true;
            this.butConsulter.Click += new System.EventHandler(this.butConsulter_Click);
            // 
            // FormGest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 429);
            this.Controls.Add(this.panel1);
            this.Name = "FormGest";
            this.Text = "Gestionnaire";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button butCalcul;
        private System.Windows.Forms.Button butSuppressionV;
        private System.Windows.Forms.Button butAjoutV;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        public System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button butLiberer;
        private System.Windows.Forms.Button butConsulter;
    }
}