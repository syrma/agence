﻿namespace Gestionnaire
{
    partial class FormInsertionV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ValidVoiture = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txbNMatricul = new System.Windows.Forms.TextBox();
            this.txbMarque = new System.Windows.Forms.TextBox();
            this.txbModel = new System.Windows.Forms.TextBox();
            this.txbTarif = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ValidVoiture
            // 
            this.ValidVoiture.Location = new System.Drawing.Point(98, 175);
            this.ValidVoiture.Name = "ValidVoiture";
            this.ValidVoiture.Size = new System.Drawing.Size(75, 23);
            this.ValidVoiture.TabIndex = 0;
            this.ValidVoiture.Text = "Valider";
            this.ValidVoiture.UseVisualStyleBackColor = true;
            this.ValidVoiture.Click += new System.EventHandler(this.ValidVoiture_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Numéro d\'immatriculation";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(106, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Marque";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(106, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Modèle";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(50, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Tarif de réservation";
            // 
            // txbNMatricul
            // 
            this.txbNMatricul.Location = new System.Drawing.Point(163, 33);
            this.txbNMatricul.Name = "txbNMatricul";
            this.txbNMatricul.Size = new System.Drawing.Size(121, 20);
            this.txbNMatricul.TabIndex = 5;
            // 
            // txbMarque
            // 
            this.txbMarque.Location = new System.Drawing.Point(163, 65);
            this.txbMarque.Name = "txbMarque";
            this.txbMarque.Size = new System.Drawing.Size(121, 20);
            this.txbMarque.TabIndex = 6;
            // 
            // txbModel
            // 
            this.txbModel.Location = new System.Drawing.Point(163, 97);
            this.txbModel.Name = "txbModel";
            this.txbModel.Size = new System.Drawing.Size(121, 20);
            this.txbModel.TabIndex = 7;
            // 
            // txbTarif
            // 
            this.txbTarif.Location = new System.Drawing.Point(163, 129);
            this.txbTarif.Name = "txbTarif";
            this.txbTarif.Size = new System.Drawing.Size(121, 20);
            this.txbTarif.TabIndex = 8;
            // 
            // FormInsertionV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 224);
            this.Controls.Add(this.txbTarif);
            this.Controls.Add(this.txbModel);
            this.Controls.Add(this.txbMarque);
            this.Controls.Add(this.txbNMatricul);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ValidVoiture);
            this.Name = "FormInsertionV";
            this.Text = "Insertion voitures";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ValidVoiture;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txbNMatricul;
        private System.Windows.Forms.TextBox txbMarque;
        private System.Windows.Forms.TextBox txbModel;
        private System.Windows.Forms.TextBox txbTarif;
    }
}