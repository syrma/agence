﻿namespace Gestionnaire
{
    partial class FormEvent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.butNon = new System.Windows.Forms.Button();
            this.butOui = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // butNon
            // 
            this.butNon.Location = new System.Drawing.Point(234, 88);
            this.butNon.Name = "butNon";
            this.butNon.Size = new System.Drawing.Size(75, 23);
            this.butNon.TabIndex = 0;
            this.butNon.Text = "Non";
            this.butNon.UseVisualStyleBackColor = true;
            this.butNon.Click += new System.EventHandler(this.butNon_Click);
            // 
            // butOui
            // 
            this.butOui.Location = new System.Drawing.Point(135, 88);
            this.butOui.Name = "butOui";
            this.butOui.Size = new System.Drawing.Size(75, 23);
            this.butOui.TabIndex = 1;
            this.butOui.Text = "Oui";
            this.butOui.UseVisualStyleBackColor = true;
            this.butOui.Click += new System.EventHandler(this.butOui_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(30, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(415, 26);
            this.label1.TabIndex = 2;
            this.label1.Text = "Voulez-vous afficher la liste des clients qui n\'ont pas restitué les voitures à t" +
    "emps ?";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.butNon);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.butOui);
            this.panel1.Location = new System.Drawing.Point(-6, -6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(463, 150);
            this.panel1.TabIndex = 3;
            // 
            // FormEvent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 136);
            this.Controls.Add(this.panel1);
            this.Name = "FormEvent";
            this.Text = "Evénement";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button butNon;
        private System.Windows.Forms.Button butOui;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
    }
}