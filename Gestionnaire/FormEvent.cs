﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Types;
using GInterface;
namespace Gestionnaire
{
    public partial class FormEvent : Form
    {

        Client[] retardataires;
        public FormEvent(Client[] retardataires)
        {
            this.retardataires = retardataires;
            InitializeComponent();            
        }

        private void butNon_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void butOui_Click(object sender, EventArgs e)
        {
            IGestionG ctl = (IGestionG)Activator.GetObject(typeof(IGestionG), "http://localhost:1069/GestClients");
            FormListeRetard flr = new FormListeRetard(retardataires, ctl);
            flr.Owner = this;
       //     this.Hide();
            flr.Show();
            flr.FormClosed += new FormClosedEventHandler(fermer);
        }

        private void fermer(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
