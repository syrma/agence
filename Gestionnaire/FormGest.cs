﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GInterface;
using Types;



namespace Gestionnaire
{
    public partial class FormGest : Form
    {
        IGestionG ctl;
        public FormGest(IGestionG ctl)
        {
            this.ctl = ctl;
            InitializeComponent();
        }

        private void butAjoutV_Click(object sender, EventArgs e)
        {
            FormInsertionV inser = new FormInsertionV(ctl);
            inser.FormClosed += new FormClosedEventHandler(butConsulter_Click);
            inser.Show();
        }

        private void butSuppressionV_Click(object sender, EventArgs e)
        {
                if (listView1.SelectedItems.Count <= 0)
                {
                    MessageBox.Show("Veuillez sélectionner une voiture.");
                    return;
                }
            try {
                //Recuperer le numero d'immatriculation d'un element selectionné 
                ListViewItem v = listView1.SelectedItems[0];
                Console.WriteLine(v.SubItems[0].Text);
                int aSup = Int32.Parse(v.SubItems[0].Text);//cast en int
                MessageBox.Show(ctl.Supprimer(aSup) ? "Suppression effectuée avec succès" : "Suppression échouée (consulter serveur)");
            
                //Actualiser la liste
                butConsulter_Click(sender, e);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("Veuillez sélectionner un élément.");

            }
        }

        private void butCalcul_Click(object sender, EventArgs e)
        {
            double recette = ctl.Recette(dateTimePicker1.Value.Date);
            if (recette == -1)
            {
                MessageBox.Show("Il n'y a pas de réservations pour ce jour (ou une erreur a eu lieu)");
                return;
            }
          MessageBox.Show ("La recette du "+ dateTimePicker1.Text + " est de : "+recette+"."); 
        }

        private void butLiberer_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count <= 0)
            {
                MessageBox.Show("Veuillez sélectionner une voiture.");
            }

            //Recuperer le numero d'immatriculation d'un element selectionné 

            else
            {
                try
                {
                    ListViewItem v = listView1.SelectedItems[0];
                    int nm = Int32.Parse(v.SubItems[0].Text);
                    Console.WriteLine(nm);
                    MessageBox.Show( ctl.Liberer(nm)?"La voiture est libre maintenant":"La voiture n'a pas pu être liberée");
                    //Actualiser la liste
                    butConsulter_Click(sender, e);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    Console.ReadLine();
                }
            }
            }

        private void butConsulter_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            Voiture[] voiture;
            voiture = ctl.ListeVoitures();
            if (voiture == null)
            {
                MessageBox.Show("Une erreur a eu lieu.");
                return;
            }
            foreach (Voiture v in voiture)
            {
                listView1.Items.Add(Convert.ToString(v.NumImmatriculation)).SubItems.AddRange(new string[] { v.Modele, v.Marque, Convert.ToString(v.Tarif),v.Etat ? "Réservée" : "Libre" });
                
           }
        }
    }
}
