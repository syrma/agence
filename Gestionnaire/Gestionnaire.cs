﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Serialization.Formatters;
using System.Windows.Forms;
using GInterface;
using Types;
using System.Collections;
namespace Gestionnaire
{
    public class Gestionnaire
    {
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                SoapClientFormatterSinkProvider clientProvider = new SoapClientFormatterSinkProvider();
                SoapServerFormatterSinkProvider serverProvider = new SoapServerFormatterSinkProvider();
                serverProvider.TypeFilterLevel = TypeFilterLevel.Full;
                IDictionary props = new Hashtable();
                props["port"] = 0;
                HttpChannel canal = new HttpChannel(props,clientProvider,serverProvider); 
				ChannelServices.RegisterChannel(canal, false);
                
                
       
                
                IGestionG ctl = (IGestionG)Activator.GetObject(typeof(IGestionG), "http://localhost:1069/GestClients");
                if (ctl == null)
                    Console.WriteLine("erreur");
                else
                {
					// inscription à l'événement des clients retardataires


	                Evenements GestionnaireEvenements = new Evenements();
					ctl.ListeClientsRetardataires += new ListeClients(GestionnaireEvenements.WrapperListeClientsRetardataires);

					GestionnaireEvenements.WrapperListeClient += ClientsRetardataires;

                    //pour executer l'IHM
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new FormGest(ctl));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadLine();
            }
        }
	     public static void ClientsRetardataires(Client[] retardataires){
            Console.WriteLine("evenement reçu");
            new FormEvent(retardataires).ShowDialog();
		}
    }


}
