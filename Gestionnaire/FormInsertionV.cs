﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GInterface;
using Types;
namespace Gestionnaire
{
    public partial class FormInsertionV : Form
    {
        IGestionG ctl;
        public FormInsertionV(IGestionG ctl)
        {
            this.ctl = ctl;
            InitializeComponent();
        }

        private void ValidVoiture_Click(object sender, EventArgs e)
        {
            try
            {
                int NM = int.Parse(txbNMatricul.Text);
                double tarif = double.Parse(txbTarif.Text);
             
                Voiture v = new Voiture(NM, txbMarque.Text, txbModel.Text, tarif, false);
                MessageBox.Show(ctl.Inserer(v) ? "Insertion réussie" : "Insertion échouée");

                this.Close();
                
            }

            catch (FormatException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("Veuillez entrer des données valides.");
            }
        }   
    }
}
