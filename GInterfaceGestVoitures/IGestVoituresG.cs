﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Types; 

namespace GInterfaceGestVoitures
{
    public interface IGestVoituresG
    {
        void Inserer(Voiture voiture);
        void Supprimer(int numImmatriculation);
    }
}
