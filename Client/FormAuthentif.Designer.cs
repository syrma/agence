﻿namespace Client
{
    partial class FormAuthentif
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.butValid = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txbTel = new System.Windows.Forms.TextBox();
            this.txbMP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // butValid
            // 
            this.butValid.Location = new System.Drawing.Point(157, 144);
            this.butValid.Name = "butValid";
            this.butValid.Size = new System.Drawing.Size(75, 23);
            this.butValid.TabIndex = 0;
            this.butValid.Text = "Valider";
            this.butValid.UseVisualStyleBackColor = true;
            this.butValid.Click += new System.EventHandler(this.butValid_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Téléphone";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(58, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mot de passe";
            // 
            // txbTel
            // 
            this.txbTel.Location = new System.Drawing.Point(157, 50);
            this.txbTel.Name = "txbTel";
            this.txbTel.Size = new System.Drawing.Size(100, 20);
            this.txbTel.TabIndex = 3;
            // 
            // txbMP
            // 
            this.txbMP.Location = new System.Drawing.Point(157, 91);
            this.txbMP.Name = "txbMP";
            this.txbMP.Size = new System.Drawing.Size(100, 20);
            this.txbMP.TabIndex = 4;
            this.txbMP.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(242, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Si vous êtes déjà inscrit, accédez à votre compte.";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // FormAuthentif
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 182);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txbMP);
            this.Controls.Add(this.txbTel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.butValid);
            this.Name = "FormAuthentif";
            this.Text = "Authentification";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button butValid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbTel;
        private System.Windows.Forms.TextBox txbMP;
        private System.Windows.Forms.Label label3;
    }
}