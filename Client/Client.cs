﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using CInterface;
using Types;
using System.Windows.Forms;
using Microsoft.VisualBasic.ApplicationServices; 

namespace Client
{
    public class Client : WindowsFormsApplicationBase
    {
        [STAThread]
        static void Main(string[] args)
        {
            try {                
                HttpChannel canal = new HttpChannel();
                ChannelServices.RegisterChannel(canal, false);
                IGestionC ctl = (IGestionC)Activator.GetObject(typeof(IGestionC), "http://localhost:1069/GestClients");
                if (ctl == null)
                    Console.WriteLine("erreur");
                else
                {
                    //Test des méthodes
					/*
                    Console.WriteLine(ctl.Reserver(1234122, 1, 3, new DateTime(2015, 11, 19)) ? "Réservation effectuée avec succès" : "Réservation échouée (consulter serveur)");
                    Console.WriteLine(ctl.Authentifier("012301231","passExample") != -1 ? "Authentification effectuée avec succès" : "Authentification échouée (consulter serveur)");
                    Console.WriteLine(ctl.Inscrire("nom","motdepasse","123123") ? "Inscription effectuée avec succès" : "Inscription échouée (consulter serveur)");
					Console.WriteLine(new DateTime());
                    Voiture[] voitures = ctl.ListeVoitures(new DateTime(2015,11,19));
					Console.WriteLine(voitures);
                    if (voitures != null)
                        foreach (Voiture voiture in voitures)
                        {
                            Console.WriteLine(voiture.NumImmatriculation + " " + voiture.Modele + " " + voiture.Marque + " " + voiture.Tarif + " " + voiture.Etat);
                        }
                    else
                        Console.WriteLine("Impossible de récupérer la liste des voitures");
					*/

                    
                    //pour executer l'IHM
					var app = new Client();
					app.EnableVisualStyles = true;
					app.ShutdownStyle = ShutdownMode.AfterAllFormsClose;
					app.MainForm = new FClient(ctl);
					app.Run(args);
                }
                }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadLine();
            }
        }
    }
}
