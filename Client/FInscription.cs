﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using CInterface;

namespace Client
{
    public partial class FInscription : System.Windows.Forms.Form 
    {
        IGestionC ctl;
        string erreurs = "";
        public FInscription(IGestionC ctl)
        {
            InitializeComponent();
            this.ctl = ctl;
        }

        private void verifierMdpasse(string mdp, string mdpconf)
        {
            if (mdp.Equals("") & mdp.Equals(""))
            {
                erreurs += "Il faut remplir les deux champs de mot de passe.\n";
                return;
            }
            if (mdp.Length < 4)
                erreurs += "Le mot de passe doit être constitué d'au moins 4 caractères.\n";
            else if (!mdp.Equals(mdpconf))
                erreurs += "Le mot de passe et la confirmation ne sont pas identiques. \n";
        }

        private void verifierNom(string nom)
        {
            if (nom.Equals(""))
                erreurs += "Vous devez entrer un nom. \n";
        }

        private void verifierNumTel(string numTel)
        {
            try
            {
                Int32.Parse(numTel);
                if (numTel.Length != 9 & numTel.Length != 10)
                    erreurs += "Le numéro de téléphone doit contenir 9 chiffres (fixe) ou 10 (mobile). \n";
            }
            catch (FormatException ex)
            {
                erreurs += "Le numéro de téléphone n'est pas valide. \n";
            }

        }
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void butValider_Click(object sender, EventArgs e)
		{ 
            string nom = txbNom.Text;
            string password = txbMPass.Text;
            string numTel = txbTel.Text ;
            string passconf = txbConfirmMPass.Text;
            verifierMdpasse(password, passconf );
            verifierNom(nom);
            verifierNumTel(numTel);
           
            if(!erreurs.Equals("")){
                MessageBox.Show(erreurs);
                erreurs = "";
                return;
            }
                    bool reussi = ctl.Inscrire(txbNom.Text, txbMPass.Text, txbTel.Text);
                    MessageBox.Show(reussi ? "Inscription réussie" : "Inscription échouée (consulter le serveur)");
                    if (!reussi)
                        return;
                        FormAuthentif ap = new FormAuthentif(ctl);
                        ap.AuthReussie += new FormAuthentif.AuthReussieEventHandler(fermer);
                        this.Hide();
                        ap.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        void fermer()
        {
            try
            {
                this.Owner.Close();
                this.Close();
            }
            catch (Exception ex) {
                Console.WriteLine(ex);
            }
        }
    }
}
