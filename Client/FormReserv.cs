﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CInterface; 
namespace Client
{
    public partial class FormReserv : System.Windows.Forms.Form
    {
        IGestionC ctl;
        int nm, id;
        DateTime d;
        public FormReserv(IGestionC ctl, int nm, int id, DateTime d)
        {
            this.ctl = ctl;
            this.nm = nm;
            this.id = id;
            this.d = d;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try { 
            int nbr = int.Parse(textBox1.Text);
            MessageBox.Show(ctl.Reserver(nm, id, nbr, d) ? "Réservation réussie" : "Réservation échouée");
            this.Close();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("Veuillez entrer un nombre valide de jours.");
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
