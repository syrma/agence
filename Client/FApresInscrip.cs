﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Types;
using CInterface;
namespace Client
{
    public partial class FApresInscrip : System.Windows.Forms.Form
    {
        IGestionC ctl;
        int id;
        public FApresInscrip(IGestionC ctl, int id)
        {
            this.ctl = ctl;
            this.id = id;
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            Voiture[] voiture;
            voiture = ctl.ListeVoitures(dateTimePicker1.Value.Date);
            foreach (Voiture v in voiture)
            {
                listView1.Items.Add(Convert.ToString(v.NumImmatriculation)).SubItems.AddRange(new string[] { v.Marque, v.Modele, Convert.ToString(v.Tarif) });
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count <= 0)
            {
                MessageBox.Show("Veuillez sélectionner une voiture.");
            }

            //Recuperer le numero d'immatriculation d'un element selectionné 

            else
            {
                try
                {
                    ListViewItem v = listView1.SelectedItems[0];
                    Console.WriteLine(v.SubItems[0].Text);
                    int nm = Int32.Parse(v.SubItems[0].Text);

                    //recuperer date du debut
                    DateTime d = dateTimePicker1.Value.Date;
                    if (d < DateTime.Today)
                    {
                        MessageBox.Show("Vous ne pouvez pas faire une réservation à une date antérieure");
                        return;
                    }

                    FormReserv fr = new FormReserv(ctl,nm,id,d);
                    fr.Show();
                    fr.FormClosed += new FormClosedEventHandler(button1_Click);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    Console.ReadLine();
                }
            }
        }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            button1_Click(sender,e);
        }
    }
}
