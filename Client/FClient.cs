﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CInterface;
using Types;



namespace Client
{
    public partial class FClient : System.Windows.Forms.Form
    {
        IGestionC ctl; 

        public FClient(IGestionC ctl)
        {
            InitializeComponent();
            this.ctl = ctl;
        }

        private void butInscription_Click(object sender, EventArgs e)
        {
            FInscription f = new FInscription(ctl);
            f.Owner = this;
            f.Show();
        }

        private void butAuthenti_Click(object sender, EventArgs e)
        {
            FormAuthentif fAut = new FormAuthentif(ctl);
            fAut.Show();
            fAut.AuthReussie += new FormAuthentif.AuthReussieEventHandler(fermer); 
        }

        private void butListeV_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            Voiture[] voiture;
           voiture= ctl.ListeVoitures(dateTimePicker1.Value.Date);
            foreach (Voiture v in voiture)
            {
                listView1.Items.Add(Convert.ToString(v.NumImmatriculation)).SubItems.AddRange(new string[] {v.Modele,v.Marque, Convert.ToString(v.Tarif)});
            }
        }

        void fermer()
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }


    

    
       
    }

    
       

    }

