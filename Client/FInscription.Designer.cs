﻿namespace Client
{
    partial class FInscription
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.butValider = new System.Windows.Forms.Button();
            this.txbConfirmMPass = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txbMPass = new System.Windows.Forms.TextBox();
            this.txbTel = new System.Windows.Forms.TextBox();
            this.txbNom = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.butValider);
            this.panel1.Controls.Add(this.txbConfirmMPass);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txbMPass);
            this.panel1.Controls.Add(this.txbTel);
            this.panel1.Controls.Add(this.txbNom);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-2, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(482, 254);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // butValider
            // 
            this.butValider.Location = new System.Drawing.Point(371, 207);
            this.butValider.Name = "butValider";
            this.butValider.Size = new System.Drawing.Size(75, 23);
            this.butValider.TabIndex = 9;
            this.butValider.Text = "Valider";
            this.butValider.UseVisualStyleBackColor = true;
            this.butValider.Click += new System.EventHandler(this.butValider_Click);
            // 
            // txbConfirmMPass
            // 
            this.txbConfirmMPass.Location = new System.Drawing.Point(211, 166);
            this.txbConfirmMPass.Name = "txbConfirmMPass";
            this.txbConfirmMPass.Size = new System.Drawing.Size(100, 20);
            this.txbConfirmMPass.TabIndex = 8;
            this.txbConfirmMPass.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(77, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Confirmer le mot de passe";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(128, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Mot de passe";
            // 
            // txbMPass
            // 
            this.txbMPass.Location = new System.Drawing.Point(211, 128);
            this.txbMPass.Name = "txbMPass";
            this.txbMPass.Size = new System.Drawing.Size(100, 20);
            this.txbMPass.TabIndex = 5;
            this.txbMPass.UseSystemPasswordChar = true;
            // 
            // txbTel
            // 
            this.txbTel.Location = new System.Drawing.Point(211, 86);
            this.txbTel.Name = "txbTel";
            this.txbTel.Size = new System.Drawing.Size(100, 20);
            this.txbTel.TabIndex = 4;
            // 
            // txbNom
            // 
            this.txbNom.Location = new System.Drawing.Point(211, 46);
            this.txbNom.Name = "txbNom";
            this.txbNom.Size = new System.Drawing.Size(100, 20);
            this.txbNom.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(128, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Téléphone ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(123, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nom complet";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(454, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vous devez vous inscrire pour pouvoir accéder à l\'ensemble des services que nous " +
    "proposons.";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // FInscription
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 253);
            this.Controls.Add(this.panel1);
            this.Name = "FInscription";
            this.Text = "FormInscription";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbTel;
        private System.Windows.Forms.TextBox txbNom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txbMPass;
        private System.Windows.Forms.TextBox txbConfirmMPass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button butValider;
    }
}