﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CInterface;
namespace Client
{
    public partial class FormAuthentif : System.Windows.Forms.Form
    {
        IGestionC ctl;

        internal delegate void AuthReussieEventHandler();
        internal event AuthReussieEventHandler AuthReussie;
        public FormAuthentif(IGestionC ctl)
        {
            InitializeComponent();
            this.ctl = ctl;
        }

        private void butValid_Click(object sender, EventArgs e)
        {
            Console.WriteLine(txbTel.Text + " " + txbMP.Text);
            int id=ctl.Authentifier(txbTel.Text, txbMP.Text);
           
            if (id != -1)
            {
                FApresInscrip api = new FApresInscrip(ctl,id);
                this.Close();
                api.Show();
                if (AuthReussie != null)
                    AuthReussie();

            }
            MessageBox.Show(id != -1 ? "Authentification réussie" : "Authentification échouée \n - Vérifiez que ces informations sont correctes. \n - Si vous n'arrivez tout de même pas à vous connecter, votre compte est peut-être bloqué.");
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        }
    }
