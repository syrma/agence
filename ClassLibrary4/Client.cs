﻿using System;

namespace Types
{
	[Serializable]
	public struct Client
	{
		private int id;
		private string nom, numTel;

		public Client(int id, string nom, string numTel){
			this.id = id; 
			this.nom = nom;
			this.numTel = numTel;
		}

		public int Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}

		public string Nom {
			get {
				return nom;
			}
			set {
				nom = value;
			}
		}


		public string NumTel {
			get {
				return numTel;
			}
			set {
				numTel = value;
			}
		}

	}
}

