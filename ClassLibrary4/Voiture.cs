﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Types
{
    [Serializable]
    public struct Voiture
    {
		public Voiture(int numImmatriculation, string marque, string modele, double tarif, bool etat){
			this.numImmatriculation = numImmatriculation;
			this.marque = marque;
			this.modele = modele;
			this.tarif = tarif;
            this.etat = etat;
		}

        public int NumImmatriculation{
            get{
                return numImmatriculation;
            }
            set
            {
                numImmatriculation = value;
            }
         }

        public string Marque
        {
            get
            {
                return marque;
            }
            set
            {
                marque = value;
            }
        }

        public string Modele {
            get
            {
                return modele;
            }
            set
            {
                modele = value;
            }
        }

        public double Tarif
        {
            get
            {
                return tarif;
            }
            set
            {
                tarif = value;
            }
        }

        public bool Etat
        {
            get
            {
                return etat;
            }
            set
            {
                etat = value;
            }
        }

        private int numImmatriculation;
        private string marque;
        private string modele;
        private double tarif;
        private bool etat;
    }


}
