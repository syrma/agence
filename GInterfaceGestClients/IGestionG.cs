﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Types;

namespace GInterface
{
	[Serializable]
	public delegate void ListeClients(Client[] Retardataires); 

	public class Evenements : MarshalByRefObject{

		public event ListeClients WrapperListeClient;

		public void WrapperListeClientsRetardataires(Client[] retardataires){
			if (WrapperListeClient != null)
				WrapperListeClient (retardataires);
		}
	}

    public interface IGestionG
    {
		bool Bloquer(int id);
        double Recette(DateTime date);
		bool Inserer(Voiture voiture);
		bool Supprimer(int numImmatriculation);

        // fonctionnalité additionnelle
        bool Liberer(int numImmatriculation);
        Voiture[] ListeVoitures();

		event ListeClients ListeClientsRetardataires; 

    }
}
